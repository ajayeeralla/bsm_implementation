### Basic Syntactiv Mutation ###

The Basic Syntactic Mutation (BSM) procedure consists of inference rules such as *Coalesce*, *Split*, *Imit*, *MuConflict*, and *ImitCycle*.


### Prerequisites ###
To run the algorithm you will need to have installed Maude, a high performance reflective language. 

### Install###
Download and install Maude from here [Download Maude](http://maude.cs.illinois.edu/w/index.php?title=The_Maude_System).

### Instructions to run the program ###
Download .maude files to a directory. Run the maude on your machine and navigate to directory where the files reside in. 
Load the file using the following command.
**Load** _file.maude_   

